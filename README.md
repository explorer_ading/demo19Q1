# 近期项目成果展示 (demo19Q1)

## 一 新闻爬虫

* 通过网络爬虫技术，每隔四个小时，按照设定的新闻种子网站，爬取数据存到云端数据库中；以下为架构图
![news-architecture](./src/news-architecture.png)

* 实现微信小程序，浏览新闻数据；
![spider-news](./src/wxmp-news.png)

* 因为目前微信平台，限制了个人开发者发布小程序（只允许企业用户），所以无法分享此小程序链接。


## 二 轻量级 JavaScript 引擎

* 独立开发一个轻量级的JavaScript 引擎，用于浏览器内核实现细节的研究 

![cricket-jscore](./src/cricket-jscore.png)

[查看源码](https://github.com/explorer-ading/cricket-jscore)



## 三 数据挖掘

* 网络爬取沪深300股指，包含的300支股票近两年内的交易信息，采用聚类算法进行数据挖掘，找出协整关系最好的配对 

![data-mining](http://www.adingx.com/wp-content/uploads/2019/04/hs300-cluster.png)

* 找出以下这对近期波动协整最好的一对

![stock-corr](http://www.adingx.com/wp-content/uploads/2019/04/pf_gd_one-1024x614.jpg)

[查看源码](./src/cluster-hs300.py)

[更多细节说明](http://www.adingx.com/quant-intro/)


## 四 计算机视觉

* 通过深度神经网络模型的泛化学习能力，迭代训练出特定的艺术风格（style image），并可应用到其他任何图像上 

![computer-vision](http://www.adingx.com/wp-content/uploads/2019/04/demo2-1024x348.png)

[查看源码](./src/pytorch_neural_style.py)

* 实现原理: 核心功能算法 Neural Algorithm 来自于这篇 [论文](https://arxiv.org/abs/1508.06576) . 功能实现上借助Facebook开源的Pytorch深度学习库，将大名鼎鼎的VGG模型做为前置泛化学习网络，后面加上绘画风格参数。
* 简单来讲，首先学出风格图片的向量值，接下来计算出输入图片向量与风格向量之间的距离，再进行一个叠加转换 (neural transfer)得出结果。
